# Создайте новый класс Airplane. Создайте следующие характеристики (полей)объекAта:
#● make (марка)
#● model
#● year
#● max_speed
#● odometer
#● is_flying
# И методы имитирующих поведение самолета take off (взлет), fly (летать), land
# приземление). Метод take off должен изменить is_flying на True, а land на False. Поз
# умолчанию поле is_flying = False. Метод fly должен принимать расстояние полета и
# изменять показания одометра (километраж). Создайте 1 объект класса и используйте все методы класса.
class Airplane():
    def __init__(self, make ,model ,year ,max_speed ,km):
        self.make = make
        self.model = model
        self.year = year
        self.max_speed = max_speed
        self.is_flying = False
        self.is_land = True
        self.odometer = 0
        self.km = km

    def take_off(self):
        self.is_flying = True
        self.is_land = False

    def fly(self):
        self.odometer += self.km

    def loading(self):
        self.is_flying = False
        self.is_land = True

    def get_status(self):

        print("make = {}  model = {}  year = {}  max_speed = {}  fly = {}  land = {}  odometer = {}  ".format(self.make , self.model , self.year , self.max_speed , self.is_flying , self.is_land , self.odometer))


Plane = Airplane ('Boing',1912,2015,900,4500)
Plane.take_off()
Plane.fly()
Plane.loading()
Plane.get_status()





